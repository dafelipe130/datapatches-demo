PCO Billing Reports Regeneration (Period only)
====================

[TOC]

## Implementation Plan

Please execute data patch: https://ops.valuex.com/index.php?task=data-patches&subtask=view&client=iol-pos-dc&patch=0171-ods.sql in order to reset the last generation date and the report will be re-generated the next day.
_(Note: The report is generated automatically in k8s based on the date of the last period. _

---

## Test Plan

This will trigger the yearly period PCO Billing Reports very next day, verify the report is generated.

---

## Backout Plan

Revert the changes made to date parameter on Period calculation from report_control table.
