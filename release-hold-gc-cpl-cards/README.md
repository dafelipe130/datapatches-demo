Datapatch to release "HOLD" from Gift Cards and CPL cards using BAMS feed
====================

[TOC]

## Implementation Plan

1. Log on to sm.shared.valuex.com

        cd /usr/local/esi/app_deploy/

1. Execute datapatch

        sudo ./datapatch.sh iolgc 2583-ods.sql prod

---

## Test Plan

Change assignee to verify the hold removal of gift cards by cross checking into GC admin page.

---

## Backout Plan

Contact change reporter
